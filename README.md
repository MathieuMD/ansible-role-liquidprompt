ansible-role-liquidprompt
=========================

Install and configure [LiquidPrompt](https://github.com/nojhan/liquidprompt)
for some or all users.

Role Variables
--------------

Define if all users must be setup (with `liquidprompt_users_all: true`) of if
only specific users (listed by `liquidprompt_users: [ root ]`).

Default LiquidPrompt configuration variables can be overrided by adding them to
`liquidprompt_custom_variables` list.

Example Playbook
----------------

```yaml
- hosts: all
  roles:
  - role: liquidprompt
```

License
-------

GPLv3

Alternatives
------------

Loosely inspired by **sbani**'s
[ansible-role-liquidprompt](https://galaxy.ansible.com/list#/roles/3254).

